document.getElementById('addInputs').addEventListener('click', function () {
    const inputDiameter = document.createElement('input');
    const inputColor = document.createElement('input');
    const buttonPaint = document.createElement('button');
    document.body.appendChild(inputDiameter);
    document.body.appendChild(inputColor);
    document.body.appendChild(buttonPaint);
    inputDiameter.placeholder = 'Write diameter of circle';
    inputColor.placeholder = 'Write color of circle';
    buttonPaint.innerText = 'Paint circles';

    buttonPaint.addEventListener('click', function () {
       const circle = document.createElement('div');
       document.body.appendChild(circle);
       circle.style.width = `${inputDiameter.value}px`;
       circle.style.height = `${inputDiameter.value}px`;
       circle.style.border = `2px solid ${inputColor.value}`;
       circle.style.borderRadius = `50%`;
       circle.style.backgroundColor = `${inputColor.value}`;


    });


});