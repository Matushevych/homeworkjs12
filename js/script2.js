document.getElementById('addInputs').addEventListener('click', function () {
    const inputDiameter = document.createElement('input');
    const buttonPaint = document.createElement('button');
    document.body.appendChild(inputDiameter);
    document.body.appendChild(buttonPaint);
    inputDiameter.placeholder = 'Write diameter of circle';
    buttonPaint.innerText = 'Paint circles';

    buttonPaint.addEventListener('click', function () {
        function random_rgba() {
            let o = Math.round, r = Math.random, s = 255;
            return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
        }

        const flexContainer = document.createElement('div');
        flexContainer.classList.add('flex-container');
        document.body.appendChild(flexContainer);
        let circle;
        for (let i = 0; i < 100; i++)
        {
            let color = random_rgba();
            circle = document.createElement('div');
            flexContainer.classList.add('flex-item');
            flexContainer.appendChild(circle);
            circle.style.width = `${inputDiameter.value - 4}px`;
            circle.style.height = `${inputDiameter.value - 4}px`;
            circle.style.border = `2px solid ${color}`;
            circle.style.backgroundColor = color;
            circle.style.flexBasis = '9%';
            circle.style.flexWrap = 'wrap';

        }
        flexContainer.addEventListener('click', function (e) {
            e.target.style.visibility = 'hidden';
            e.stopPropagation();
        })

    });


});